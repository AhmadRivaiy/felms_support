import React, { lazy } from 'react'
import { Link } from 'react-router-dom';

class Home extends React.Component {
    constructor(props) {
      super(props);
  
      this.state = {
          users: null
      };
    }

    render(){
        return(
            <div style={{ display: 'flex', justifyContent: 'center', alignItems: 'center', flexDirection: 'column', height: '100vh'}}>
                <Link to="/ujian_online">
                    <div>Ujian Online</div>
                </Link>
            </div>
        )
    }
}

export default Home;
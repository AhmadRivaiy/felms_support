import React from 'react'
import Row from 'react-bootstrap/Row'
import Col from 'react-bootstrap/Col'
import Card from 'react-bootstrap/Card'
import ProgressBar from 'react-bootstrap/ProgressBar'
import Button from 'react-bootstrap/Button'
import Form from 'react-bootstrap/Form'
import ListGroup from 'react-bootstrap/ListGroup'
import { AiOutlineCheck, AiOutlineClose, AiOutlineTag } from "react-icons/ai";
import './index.scss';
import { Badge } from 'react-bootstrap'

class UjianOnline extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            users: null,
            leftOpen: true,
            rightOpen: false,
            activeTaks: 'vl',
            inactiveTaks: ''
        };
    }

    toggleSidebar = (event) => {
        let key = `${event.currentTarget.parentNode.id}Open`;
        this.setState({ [key]: !this.state[key] });
    }

    toggleSidebarDiv(event){
        let key = event + 'Open';
        this.setState({ [key]: !this.state[key] });
    }

    render() {
        let leftOpen = this.state.leftOpen ? 'open' : 'closed';
        let rightOpen = this.state.rightOpen ? 'open' : 'closed';
        const { activeTaks, inactiveTaks } = this.state;
        return (
            <div id='layout'>

                <div id='left' className={leftOpen}>
                    <div className='icon' onClick={this.toggleSidebar} >
                        &equiv;
                    </div>
                    <div className={`sidebar ${leftOpen}`} >
                        <div className='header' />
                        <div className='content left-outline'>
                            <h3 style={{ marginTop: 5, marginBottom: 25 }}>List Agenda</h3>
                            <h6>Semua Agenda</h6>
                            <ListGroup>
                                <ListGroup.Item className="list-ujian" action active={true}>
                                    <Row>
                                        <Col lg={10}>Semua</Col>
                                        <Col lg={2} style={{ textAlign: 'end' }}><i><AiOutlineCheck /></i></Col>
                                    </Row>
                                </ListGroup.Item>
                                <ListGroup.Item className="list-ujian" action active={false}>
                                    <Row>
                                        <Col lg={8}>Ujian</Col>
                                        <Col lg={4} style={{ textAlign: 'end' }}><i><AiOutlineCheck /></i></Col>
                                    </Row>
                                </ListGroup.Item>
                                <ListGroup.Item className="list-ujian" action active={false}>
                                    <Row>
                                        <Col lg={8}>Tugas</Col>
                                        <Col lg={4} style={{ textAlign: 'end', }}><i><AiOutlineCheck /></i></Col>
                                    </Row>
                                </ListGroup.Item>
                            </ListGroup>
                        </div>
                    </div>
                </div>

                <div id='main'>
                    <div className='header'>
                        <h3 className={`
                        title
                        ${'left-' + leftOpen}
                        ${'right-' + rightOpen}
                    `}>
                        </h3>
                    </div>
                    <div className='content'>
                        <Form style={{ marginBottom: 50 }}>
                            <Form.Row>
                                <Col md={6} sm={5}>
                                    <Form.Control
                                        className="mb-2 mr-sm-2"
                                        id="inlineFormInputName2"
                                        placeholder="Tambah Task..."
                                    />
                                </Col>
                                <Col lg={2} md={2} sm={2} style={{ textAlign: 'end' }} xs={6}>
                                    <Button type="submit" className="mb-2">
                                        Submit
                                    </Button>
                                </Col>
                                <Col lg={1} md={1} sm={1} xs={6}>
                                    <Button type="clear" variant="secondary" className="mb-2">
                                        Cancel
                                    </Button>
                                </Col>
                            </Form.Row>
                        </Form>
                        <div className="divTaks" onClick={() => this.toggleSidebarDiv('right')}>
                            <Row>
                                <Col xs={1}>
                                    <div className={activeTaks}></div>
                                </Col>
                                <Col xs={1} className="centerItems">
                                    <div><Badge variant="primary">ID6851</Badge></div>
                                </Col>
                                <Col xs={4} className="centerItems">
                                    <div>Ini Adalah...</div> { /* Batas Kalimat ini sebanyak 10 Huruf Saja untuk hasil terbaik di hp */}
                                </Col>
                                <Col xs={3} className="centerItems" style={{ flexDirection: 'row' }}>
                                    <div style={{ color: '#ff6666'}}><i><AiOutlineTag/></i> Tags;</div>
                                    <div style={{ color: '#ff6666' }}><i><AiOutlineTag/></i> Tags;</div>
                                </Col>
                                <Col xs={3} style={{ marginTop: 17.5 }} className="d-none d-sm-block">
                                    <ProgressBar animated now={75} />
                                </Col>
                            </Row>
                        </div>
                        <div className="divTaks" onClick={() => this.toggleSidebarDiv('right')}>
                            <Row>
                                <Col xs={1}>
                                    <div className={inactiveTaks}></div>
                                </Col>
                                <Col xs={1} className="centerItems">
                                    <div><Badge variant="primary">ID6852</Badge></div>
                                </Col>
                                <Col xs={4} className="centerItems">
                                    <div>Ini Adalah Taks...</div>
                                </Col>
                                <Col xs={3} className="centerItems" style={{ flexDirection: 'row' }}>
                                    <div style={{ color: '#ff6666'}}><i><AiOutlineTag/></i> Tags;</div>
                                    <div style={{ color: '#ff6666' }}><i><AiOutlineTag/></i> Tags;</div>
                                </Col>
                                <Col xs={3} style={{ marginTop: 17.5 }} className="d-none d-sm-block">
                                    <ProgressBar animated now={45} />
                                </Col>
                            </Row>
                        </div>
                        <div className="divTaks" onClick={() => this.toggleSidebarDiv('right')}>
                            <Row>
                                <Col xs={1}>
                                    <div className={inactiveTaks}></div>
                                </Col>
                                <Col xs={1} className="centerItems">
                                    <div><Badge variant="primary">ID6853</Badge></div>
                                </Col>
                                <Col xs={4} className="centerItems">
                                    <div>Ini sdfdsfdsdfdf...</div>
                                </Col>
                                <Col xs={3} className="centerItems" style={{ flexDirection: 'row' }}>
                                    <div style={{ color: '#ff6666'}}><i><AiOutlineTag/></i> Tags;</div>
                                    <div style={{ color: '#ff6666' }}><i><AiOutlineTag/></i> Tags;</div>
                                </Col>
                                <Col xs={3} style={{ paddingTop: 17}} className="d-none d-sm-block">
                                    <ProgressBar animated now={5} />
                                </Col>
                            </Row>
                        </div>
                        <div className="divTaks" onClick={() => this.toggleSidebarDiv('right')}>
                            <Row>
                                <Col xs={1}>
                                    <div className={activeTaks}></div>
                                </Col>
                                <Col xs={1} className="centerItems">
                                    <div><Badge variant="primary">ID6854</Badge></div>
                                </Col>
                                <Col xs={4} className="centerItems">
                                    <div>Ini Adalah</div>
                                </Col>
                                <Col xs={3} className="centerItems" style={{ flexDirection: 'row' }}>
                                    <div style={{ color: '#ff6666'}}><i><AiOutlineTag/></i> Tags; </div>
                                    <div style={{ color: '#ff6666' }}><i><AiOutlineTag/></i> Tags;</div>
                                </Col>
                                <Col xs={3} style={{ marginTop: 17.5 }} className="d-none d-sm-block">
                                    <ProgressBar animated now={100} />
                                </Col>
                            </Row>
                        </div>
                    </div>
                </div>

                <div id='right' className={rightOpen} >
                    <div className='header'>
                        <h3 className="text-right" onClick={() => this.toggleSidebarDiv('right')}>
                            <i><AiOutlineClose className="testClose"/></i>
                        </h3>
                    </div>
                    <div className={`sidebar ${rightOpen}`} >
                        <div className='header'>
                        </div>
                        <div className='content'>
                            <h3>Right content</h3><br />
                            <p>
                                Mauris velit turpis, scelerisque at velit sed, porta varius tellus. Donec mollis faucibus arcu id luctus. Etiam sit amet sem orci. Integer accumsan enim id sem aliquam sollicitudin. Etiam sit amet lorem risus. Aliquam pellentesque vestibulum hendrerit. Pellentesque dui mauris, volutpat vel sapien vitae, iaculis venenatis odio. Donec vel metus et purus ullamcorper consequat. Mauris at ullamcorper quam, sed vehicula felis. Vestibulum fringilla, lacus sit amet finibus imperdiet, tellus massa pretium urna, non lacinia dui nibh ut enim. Nullam vestibulum bibendum purus convallis vehicula. Morbi tempor a ipsum mattis pellentesque. Nulla non libero vel enim accumsan luctus.
                        </p>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}

export default UjianOnline;